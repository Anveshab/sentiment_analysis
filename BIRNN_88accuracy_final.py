
# coding: utf-8

# In[1]:



# In[1]:

import pandas as pd
import nltk
from nltk.corpus import stopwords
import enchant
import numpy as np


# In[2]:

data = pd.read_csv('/home/valiance/Desktop/sentiment _analysis/training_data_3classes.csv',encoding="utf8",header=0)
train_data = data.iloc[0:4915,0]
train_target = data.iloc[0:4915,1]
#test_data =  data.iloc[4501:4915,0]
#test_target =  data.iloc[4501:4915,1]


# In[3]:

word=[]
for i in range(len(train_data)):
        k=train_data.iloc[i].split(',')
        word.append(k)


# In[4]:

vocab=[]
for i in word:
    for k in i:
        vocab.append(k.lower())


# In[ ]:

stop = stopwords.words('english')


# In[ ]:

#for k in vocab:
        #if k in stop:
            #vocab.remove(k)           


operators = set(("not","no","don't","wouldn't","didn't","couldn't","shouldn't","very","aren't","can't","doesn't","hadn't","wasn't","nothing","hasn't","without","haven't","isn't","nor",'cannot','neither',"too","won't"))
#stop = set(stopwords) - operators
stop = set(stopwords.words('english'))-operators


# In[ ]:
for k in vocab:
        if k in stop:
                vocab.remove(k) 

ult=[]
for k in vocab:
    if k not in ult:
        ult.append(k)


# In[6]:

new_vocab = ult
new_vocab.remove('')
d = enchant.Dict("en_US")


# In[7]:

for i in new_vocab:
    tem = d.check(i)
    if tem is True:
        new_vocab.remove(i)
dic={}
k=0
for i in range(len(new_vocab)):
    dic[new_vocab[i]]=k
    k=k+1


# In[ ]:

dic={}
k=0
for i in range(len(new_vocab)):
    dic[new_vocab[i]]=k
    k=k+1


# In[8]:

target=[]
for i in train_target:
    target.append(i)
for i in range(len(word)):
    temp=word[i]
    for n in temp:
        pos=temp.index(n)
        if n in dic:
            temp[pos]=dic[n]
        else:
            temp[pos]=k
            dic[n]=k
            k=k+1

    word[i]=temp    


# In[ ]:

for i in range(len(word)):
    temp=word[i]
    for n in temp:
        pos=temp.index(n)
        if n in dic:
            temp[pos]=dic[n]
        else:
            temp[pos]=k
            dic[n]=k
            k=k+1

    word[i]=temp


# In[9]:

column = ["Review","Rating"]
final_train = pd.DataFrame(columns=column)


# In[10]:

tempo = pd.DataFrame


# In[11]:

for i in range(len(final_train['Review'])):
    a = final_train['Review']
    #print(a)
    


# In[12]:

for i in range(len(word)):
    final_train.loc[i,'Review']=word[i]


# In[13]:

def dense_to_one_hot(labels_dense, num_classes):
    num_labels = labels_dense.shape[0]
    index_offset = np.arange(num_labels) * num_classes
    labels_one_hot = np.zeros((num_labels, num_classes))
    labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
    return labels_one_hot


# In[14]:

final_train_target = dense_to_one_hot(train_target.values,3)

# In[15]:

for i in range(len(target)):
    final_train.loc[i,'Rating']=final_train_target[i]


# In[16]:

maximum=0
for i in final_train['Review']:
    tmp1=i
    if len(tmp1)>maximum:
        maximum=len(tmp1)


# In[17]:

# padding

for i1 in final_train['Review']:
    tmp2=i1
    if len(tmp2)<maximum:
        num=maximum-len(tmp2)
        for i in range(num):
            tmp2.append(0)
for i1 in final_train['Review']:
    tmp2=i1
    if len(tmp2)<784:
        num=784-len(tmp2)
        for i in range(num):
            tmp2.append(0)


# In[2]:

t_data = final_train['Review'].values
t_labels = final_train['Rating'].values


# In[3]:

from __future__ import print_function

import tensorflow as tf
from tensorflow.contrib import rnn

# Import MNIST data

'''
To classify images using a recurrent neural network, we consider every image
row as a sequence of pixels. Because MNIST image shape is 28*28px, we will then
handle 28 sequences of 28 steps for every sample.
'''

# Parameters
learning_rate = 1e-4
training_iters = 10000
batch_size = 128
display_step = 10

# Network Parameters
n_input = 28# MNIST data input (img shape: 28*28)
n_steps = 28 # timesteps
n_hidden = 500# hidden layer num of features
n_classes = 3# MNIST total classes (0-9 digits)

# tf Graph input
x = tf.placeholder("float", [None, n_steps, n_input])
y = tf.placeholder("float", [None, n_classes])

# Define weights


# In[4]:

# Define weights
weights = {
    # Hidden layer weights => 2*n_hidden because of forward + backward cells
    'out': tf.Variable(tf.random_normal([2*n_hidden, n_classes]))
}
biases = {
    'out': tf.Variable(tf.random_normal([n_classes]))
}


# In[5]:

num_layers=5
def BiRNN(x, weights, biases):

    # Prepare data shape to match `bidirectional_rnn` function requirements
    # Current data input shape: (batch_size, n_steps, n_input)
    # Required shape: 'n_steps' tensors list of shape (batch_size, n_input)

    # Permuting batch_size and n_steps
    x = tf.transpose(x, [1, 0, 2])
    # Reshape to (n_steps*batch_size, n_input)
    x = tf.reshape(x, [-1, n_input])
    # Split to get a list of 'n_steps' tensors of shape (batch_size, n_input)
    x = tf.split(x, n_steps, 0)

    # Define lstm cells with tensorflow
    # Forward direction cell
    #lstm_fw_cell = rnn.BasicLSTMCell(n_hidden, forget_bias=1.0)
    fw_cell = tf.contrib.rnn.LSTMCell(n_hidden, state_is_tuple=True,activation=tf.nn.tanh) 
    cell1 = tf.contrib.rnn.MultiRNNCell([fw_cell] * num_layers, state_is_tuple=True)
    # Backward direction cell
    #lstm_bw_cell = rnn.BasicLSTMCell(n_hidden, forget_bias=1.0)
    bw_cell = tf.contrib.rnn.LSTMCell(n_hidden, state_is_tuple=True,activation=tf.nn.tanh) 
    cell2 = tf.contrib.rnn.MultiRNNCell([bw_cell] * num_layers, state_is_tuple=True)

    # Get lstm cell output
    try:
        outputs, _, _ = rnn.static_bidirectional_rnn(cell1, cell2, x,
                                              dtype=tf.float32)
    except Exception: # Old TensorFlow version only returns outputs not states
        outputs = rnn.static_bidirectional_rnn(cell1, cell2, x,
                                        dtype=tf.float32)

    # Linear activation, using rnn inner loop last output
    return tf.matmul(outputs[-1], weights['out']) + biases['out']


# In[6]:

pred = BiRNN(x, weights, biases)


# In[7]:

epochs_completed = 0
index_in_epoch = 0
num_examples = 4000

# serve data by batches
def next_batch(batch_size):
    
    global t_data
    global t_labels
    global index_in_epoch
    global epochs_completed
    
    start = index_in_epoch
    index_in_epoch += batch_size
    
    
    # when all trainig data have been already used, it is reorder randomly    
    if index_in_epoch > num_examples:
        # finished epoch
        epochs_completed += 1
        # shuffle the data
        perm = np.arange(num_examples)
        np.random.shuffle(perm)
        train_images = t_data[perm]
        train_labels = t_labels[perm]
        # start next epoch
        start = 0
        index_in_epoch = batch_size
        assert batch_size <= num_examples
    end = index_in_epoch
    #print(t_data[start:end])
    print(type(t_data[start:end]))
    print(len(t_data[start:end]))
    print(len(t_labels[start:end]))
    #print(type(t_labels[start:end]))
    return t_data[start:end], t_labels[start:end]


# In[8]:

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Evaluate model
correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()
#init = tf.initialize_all_variables()


# In[10]:

from numpy import array
training_iters=50000
with tf.Session() as sess:
    sess.run(init)
    step = 1
    #print((type(next_batch(batch_size))))
    #print((next_batch(batch_size)))
    batch_x, batch_y = next_batch(batch_size)
    #print(type(batch_y))
    #print(batch_x)
    #print(batch_x)
    # Keep training until reach max iterations
    while step * batch_size < training_iters:
        batch_x, batch_y = next_batch(batch_size)
        #print(type(batch_x))
        #print(len(batch_x))
        #print(len(batch_y))
        #print(len(batch_x[1]))
        #print(batch_y)
        # Reshape data to get 28 seq of 28 elements
        result=[]
        for i in range(len(batch_x)):
            a2=array(batch_x[i])
            a3=a2.reshape(28,28)
            result.append(a3)
        myarray = np.asarray(result)
        result1=[]
        for i in range(len(batch_y)):
            a2=array(batch_y[i])
            result1.append(a2)
        myarray1 = np.asarray(result1)
        #print(type(myarray))
        #print(len(myarray))
        #print(len(myarray[1][1]))
        #print((myarray1))
        #batch_x1 = batch_x.reshape((28,28))
         # Run optimization op (backprop)
        sess.run(optimizer, feed_dict={x: myarray, y: myarray1})
        if step % display_step == 0:
            # Calculate batch accuracy
            acc = sess.run(accuracy, feed_dict={x: myarray, y: myarray1})
            # Calculate batch loss
            loss = sess.run(cost, feed_dict={x: myarray, y: myarray1})
            print("Iter " + str(step*batch_size) + ", Minibatch Loss= " +                   "{:.6f}".format(loss) + ", Training Accuracy= " +                   "{:.5f}".format(acc))
        step += 1
    print("Optimization Finished!")

    #Calculate accuracy for 128 mnist test images
    

