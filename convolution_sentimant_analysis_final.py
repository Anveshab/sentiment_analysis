
# coding: utf-8

# In[4]:




# In[1]:

import pandas as pd
import nltk
from nltk.corpus import stopwords
import enchant
import numpy as np


# In[2]:

data = pd.read_csv('/home/valiance/Desktop/sentiment _analysis/training_data_1_2.csv',encoding="utf8",header=0)
train_data = data.iloc[0:4000,0]
train_target = data.iloc[0:4000,1]
test_data =  data.iloc[4001:4355,0]
test_target =  data.iloc[4001:4355,1]


# In[3]:

word=[]
for i in range(len(train_data)):
        k=train_data.iloc[i].split(',')
        word.append(k)


# In[4]:

vocab=[]
for i in word:
    for k in i:
        vocab.append(k.lower())


# In[ ]:

#stop = stopwords.words('english')


# In[ ]:

#for k in vocab:
        #if k in stop:
            #vocab.remove(k)           


#operators = set(("not","no","don't","wouldn't","didn't","couldn't","shouldn't","very","aren't","can't","doesn't","hadn't","wasn't","nothing","hasn't","without","haven't","isn't","nor",'cannot','neither',"too","won't"))
#stop = set(stopwords) - operators
#stop = set(stopwords.words('english'))-operators


# In[ ]:

#for k in vocab:
     #   if k in stop:
       #    vocab.remove(k)  

ult=[]
for k in vocab:
    if k not in ult:
        ult.append(k)


# In[6]:

new_vocab = ult
new_vocab.remove('')
d = enchant.Dict("en_US")


# In[7]:

for i in new_vocab:
    tem = d.check(i)
    if tem is True:
        new_vocab.remove(i)
dic={}
k=0
for i in range(len(new_vocab)):
    dic[new_vocab[i]]=k
    k=k+1


# In[ ]:

dic={}
k=0
for i in range(len(new_vocab)):
    dic[new_vocab[i]]=k
    k=k+1


# In[8]:

target=[]
for i in train_target:
    target.append(i)
for i in range(len(word)):
    temp=word[i]
    for n in temp:
        pos=temp.index(n)
        if n in dic:
            temp[pos]=dic[n]
        else:
            temp[pos]=k
            dic[n]=k
            k=k+1

    word[i]=temp    


# In[ ]:

for i in range(len(word)):
    temp=word[i]
    for n in temp:
        pos=temp.index(n)
        if n in dic:
            temp[pos]=dic[n]
        else:
            temp[pos]=k
            dic[n]=k
            k=k+1

    word[i]=temp


# In[9]:

column = ["Review","Rating"]
final_train = pd.DataFrame(columns=column)


# In[10]:

tempo = pd.DataFrame


# In[11]:

for i in range(len(final_train['Review'])):
    a = final_train['Review']
    #print(a)
    


# In[12]:

for i in range(len(word)):
    final_train.loc[i,'Review']=word[i]


# In[13]:

def dense_to_one_hot(labels_dense, num_classes):
    num_labels = labels_dense.shape[0]
    index_offset = np.arange(num_labels) * num_classes
    labels_one_hot = np.zeros((num_labels, num_classes))
    labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
    return labels_one_hot


# In[14]:

final_train_target = dense_to_one_hot(train_target.values,5)


# In[15]:

for i in range(len(target)):
    final_train.loc[i,'Rating']=final_train_target[i]


# In[16]:

maximum=0
for i in final_train['Review']:
    tmp1=i
    if len(tmp1)>maximum:
        maximum=len(tmp1)


# In[17]:

# padding

for i1 in final_train['Review']:
    tmp2=i1
    if len(tmp2)<maximum:
        num=maximum-len(tmp2)
        for i in range(num):
            tmp2.append(0)
for i1 in final_train['Review']:
    tmp2=i1
    if len(tmp2)<784:
        num=784-len(tmp2)
        for i in range(num):
            tmp2.append(0)


# In[5]:

t_data = final_train['Review'].values
t_labels = final_train['Rating'].values


# In[6]:

print(t_labels)


# In[7]:

epochs_completed = 0
index_in_epoch = 0
num_examples = 4000

# serve data by batches
def next_batch(batch_size):
    
    global t_data
    global t_labels
    global index_in_epoch
    global epochs_completed
    
    start = index_in_epoch
    index_in_epoch += batch_size
    
    # when all trainig data have been already used, it is reorder randomly    
    if index_in_epoch > num_examples:
        # finished epoch
        epochs_completed += 1
        # shuffle the data
        perm = np.arange(num_examples)
        np.random.shuffle(perm)
        train_images = t_data[perm]
        train_labels = t_labels[perm]
        # start next epoch
        start = 0
        index_in_epoch = batch_size
        assert batch_size <= num_examples
    end = index_in_epoch
    #print(t_data[start:end])
    print(type(t_data[start:end]))
    print(len(t_data[start:end]))
    print(len(t_labels[start:end]))
    #print(type(t_labels[start:end]))
    return t_data[start:end], t_labels[start:end]


# In[8]:

'''
A Convolutional Network implementation example using TensorFlow library.
This example is using the MNIST database of handwritten digits
(http://yann.lecun.com/exdb/mnist/)
Author: Aymeric Damien
Project: https://github.com/aymericdamien/TensorFlow-Examples/
'''
from numpy import array

from __future__ import print_function

import tensorflow as tf

# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
#mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

# Parameters
learning_rate = 0.001
training_iters = 60000
batch_size = 128
display_step = 20

# Network Parameters
n_input = 784 # MNIST data input (img shape: 28*28)
n_classes = 5 # MNIST total classes (0-9 digits)
dropout = 0.75 # Dropout, probability to keep units

# tf Graph input
x = tf.placeholder(tf.float32, [None, n_input])
y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32) #dropout (keep probability)


# Create some wrappers for simplicity
def conv2d(x, W, b, strides=1):
    # Conv2D wrapper, with bias and relu activation
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
    return tf.nn.relu(x)


def maxpool2d(x, k=2):
    # MaxPool2D wrapper
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],
                          padding='SAME')


# Create model
def conv_net(x, weights, biases, dropout):
    # Reshape input picture
    x = tf.reshape(x, shape=[-1, 28, 28, 1])

    # Convolution Layer
    conv1 = conv2d(x, weights['wc1'], biases['bc1'])
    # Max Pooling (down-sampling)
    conv1 = maxpool2d(conv1, k=2)

    # Convolution Layer
    conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
    # Max Pooling (down-sampling)
    conv2 = maxpool2d(conv2, k=2)

    # Fully connected layer
    # Reshape conv2 output to fit fully connected layer input
    fc1 = tf.reshape(conv2, [-1, weights['wd1'].get_shape().as_list()[0]])
    fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
    fc1 = tf.nn.relu(fc1)
    # Apply Dropout
    fc1 = tf.nn.dropout(fc1, dropout)

    # Output, class prediction
    out = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
    return out

# Store layers weight & bias
weights = {
    # 5x5 conv, 1 input, 32 outputs
    'wc1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
    # 5x5 conv, 32 inputs, 64 outputs
    'wc2': tf.Variable(tf.random_normal([5, 5, 32, 64])),
    # fully connected, 7*7*64 inputs, 1024 outputs
    'wd1': tf.Variable(tf.random_normal([7*7*64, 1024])),
    # 1024 inputs, 10 outputs (class prediction)
    'out': tf.Variable(tf.random_normal([1024, n_classes]))
}

biases = {
    'bc1': tf.Variable(tf.random_normal([32])),
    'bc2': tf.Variable(tf.random_normal([64])),
    'bd1': tf.Variable(tf.random_normal([1024])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

# Construct model
pred = conv_net(x, weights, biases, keep_prob)

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Evaluate model
correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.initialize_all_variables()
# Launch the graph


# In[9]:

with tf.Session() as sess:
    sess.run(init)
    step = 1
    # Keep training until reach max iterations
    while step * batch_size < training_iters:
        batch_x, batch_y = next_batch(batch_size)
        print(type((batch_x)))
        result2=[]
        for i in range(len(batch_x)):
            a2=array(batch_x[i])
            result2.append(a2)
        myarray2 = np.asarray(result2)

        result1=[]
        for i in range(len(batch_y)):
            a2=array(batch_y[i])
            result1.append(a2)
        myarray1 = np.asarray(result1)
        print(len(myarray1[1]))
        print(len(myarray2[1]))
        # Run optimization op (backprop)
        sess.run(optimizer, feed_dict={x: myarray2, y: myarray1,
                                           keep_prob: dropout})
        if step % display_step == 0:
            # Calculate batch loss and accuracy
            loss, acc = sess.run([cost, accuracy], feed_dict={x: myarray2,
                                                              y: myarray1,
                                                              keep_prob: 1.})
            print("Iter " + str(step*batch_size) + ", Minibatch Loss= " +                   "{:.6f}".format(loss) + ", Training Accuracy= " +                   "{:.5f}".format(acc))
        step += 1
    print("Optimization Finished!")

    # Calculate accuracy for 256 mnist test images
    #print("Testing Accuracy:", \
        #sess.run(accuracy, feed_dict={x: mnist.test.images[:256],
         #                             y: mnist.test.labels[:256],
#keep_prob: 1.}))


# In[10]:

w1 = tf.Variable(tf.truncated_normal(shape=[10]), name='w1')
w2 = tf.Variable(tf.truncated_normal(shape=[20]), name='w2')
tf.add_to_collection('vars', w1)
tf.add_to_collection('vars', w2)
saver = tf.train.Saver()
sess = tf.Session()
sess.run( tf.initialize_all_variables())
saver.save(sess, 'my-model_1')


# In[11]:

sess = tf.Session()
new_saver = tf.train.import_meta_graph('my-model_1.meta')
new_saver.restore(sess, tf.train.latest_checkpoint('./'))
all_vars = tf.get_collection('vars')
for v in all_vars:
    v_ = sess.run(v)
    print(v_)

